import {createSlice} from "@reduxjs/toolkit";


const initialState = {
    favorite: JSON.parse(localStorage.getItem('favorite')) || [],
    basket: JSON.parse(localStorage.getItem('basket')) || [],
};

const counterReducer = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        addToFav: (state, action) => {
            state.favorite.push(action.payload);
            localStorage.setItem('favorite', JSON.stringify(state.favorite));
        },
        addToBasket: (state, action) => {
            state.basket.push(action.payload);
            localStorage.setItem('basket', JSON.stringify(state.basket));
        },
        deleteToFAv: (state, action) => {
            state.favorite = state.favorite.filter(item => item.id !== action.payload);
            localStorage.setItem('favorite', JSON.stringify(state.favorite));
        },
        deleteToBasket: (state, action) => {
            state.basket = state.basket.filter(item => item.id !== action.payload);
            localStorage.setItem('basket', JSON.stringify(state.basket));
        },
    },
});

export const {addToFav, addToBasket, deleteToFAv, deleteToBasket} = counterReducer.actions;
export default counterReducer;