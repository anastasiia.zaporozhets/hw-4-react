import ModalImage from "../../components/ModalComponents/ModalImage/ModalImage.jsx";
import {useEffect} from "react";
import Slider from "../../components/Slider/Slider.jsx";
import ProductsList from "../../components/ProductsComponents/ProductList/ProductList.jsx";
import {useDispatch, useSelector} from "react-redux";
import {productsFetch} from "../../features/content/productsSlice.js";
import {addToBasket} from "../../features/counter/counterSlice.js";
import {closeModal, openModal} from "../../features/modal/modalSlice.js";
import "../MainPage/MainPage.scss"


function MainPage() {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(productsFetch())

    }, [dispatch])

    const data = useSelector((state) => state.card.products);
    const modal = useSelector(state => state.modals);
    const basket = useSelector(state => state.counter.basket);

    function openModalHandler(modalType, itemData) {
        dispatch(openModal({modalType, modalData: itemData}))
        console.log("Modal", itemData)
    }

    const addToBasketHandler = () => {
        if (!basket.find(item => item.id === modal.modalData.id)) {
            dispatch(addToBasket(modal.modalData));
            dispatch(closeModal());
        } else {
            alert("Цей товар доданий раніше в корзину!!!");
        }
    };


    return (
        <>
            <Slider/>

            <section className="product-list">
                <h1 className="product-list__title">Тактичний одяг для чоловіків</h1>
                <ProductsList items={data}
                              onBuyClick={(itemData) => openModalHandler("image", itemData)}

                />
            </section>

            {modal.isModalOpen && modal.modalType === "image" && (
                <ModalImage
                    data={modal.modalData}
                    bodyQuestion={<p className="modal-body__text ">ДОДАТИ ТОВАР ДО КОШИКА? </p>}
                    onConfirm={addToBasketHandler}
                />
            )}

        </>
    )
}

export default MainPage;