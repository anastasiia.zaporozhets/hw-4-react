import "../FavoritePage/FavoritePage.scss";
import {useDispatch} from "react-redux";
import {useSelector} from "react-redux";
import ProductsList from "../../components/ProductsComponents/ProductList/ProductList.jsx";
import {openModal, closeModal} from "../../features/modal/modalSlice.js";
import {addToBasket} from "../../features/counter/counterSlice.js";
import ModalImage from "../../components/ModalComponents/ModalImage/ModalImage.jsx";


function FavoritesPage() {

    const dispatch = useDispatch();
    const favorite = useSelector(state => state.counter.favorite);
    const basket = useSelector(state => state.counter.basket)

    const modal = useSelector(state => state.modals)
    const addToBasketHandler = () => {
        if (!basket.find(item => item.id === modal.modalData.id)) {
            dispatch(addToBasket(modal.modalData));
            dispatch(closeModal());
        } else {
            alert("Цей товар доданий раніше в корзину!!!");
        }
    };

    function openModalHandler(modalType, itemData) {
        dispatch(openModal({modalType, modalData: itemData}))
        console.log("Modal", itemData)
    }

    return (
        <section className="favorite-page">
            <h1 className="favorite-page__title">СПИСОК БАЖАНЬ</h1>
            <ProductsList items={favorite}
                          onBuyClick={(itemData) => openModalHandler("image", itemData)}
                          showStar={true}
                          showDelete={false}/>

            {modal.isModalOpen && modal.modalType === "image" && (
                <ModalImage
                    data={modal.modalData}
                    bodyQuestion={<p className="modal-body__text ">ДОДАТИ ТОВАР ДО КОШИКА? </p>}
                    onConfirm={addToBasketHandler}
                />
            )}
        </section>
    );
}

export default FavoritesPage;