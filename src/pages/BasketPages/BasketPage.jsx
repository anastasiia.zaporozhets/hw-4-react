import ModalImage from "../../components/ModalComponents/ModalImage/ModalImage.jsx";
import "../BasketPages/BasketPage.scss"
import {useDispatch} from "react-redux";
import {useSelector} from "react-redux";
import {closeModal, openModal} from "../../features/modal/modalSlice.js";
import {deleteToBasket} from "../../features/counter/counterSlice.js";
import ProductsList from "../../components/ProductsComponents/ProductList/ProductList.jsx";


function BasketPage() {
    const dispatch = useDispatch();


    const basket = useSelector(state => state.counter.basket)
    console.log(basket)

    const modal = useSelector(state => state.modals);

    const confirmDeletionHandler = () => {
        dispatch(deleteToBasket(modal.modalData.id));
        console.log("так видалити", modal)
    };

    function handlerBuy() {
        console.log("Куплено")
    }

    return (
        <section className="basket-page">
            <h1 className="basket-page__title">ДОДАНІ ТОВАРИ В КОШИК</h1>
            <ProductsList
                items={basket}
                onBuyClick={handlerBuy}
                showStar={true}
                showDelete={true}
                showButtonBuy={false}
            />
            {modal.isModalOpen && modal.modalType === "image" && (
                <ModalImage
                    data={modal.modalData}
                    bodyQuestion={<p className="modal-body__text">ВИДАЛИТИ ТОВАР З КОШИКА?</p>}
                    onConfirm={confirmDeletionHandler}
                    onClose={() => dispatch(closeModal())}
                />
            )}
        </section>
    );
}

export default BasketPage;