import {configureStore} from "@reduxjs/toolkit";
import productsSlice from "../features/content/productsSlice.js";
import counterSlice from "../features/counter/counterSlice.js";
import modalSlice from "../features/modal/modalSlice.js";


const store = configureStore({
    reducer: {
        card: productsSlice.reducer,
        counter: counterSlice.reducer,
        modals: modalSlice.reducer
    }
})

export default store;
