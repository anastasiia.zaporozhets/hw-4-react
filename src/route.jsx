import {createBrowserRouter} from "react-router-dom";
import Layout from "./Layout.jsx";
import MainPage from "./pages/MainPage/MainPage.jsx";
import BasketPage from "./pages/BasketPages/BasketPage.jsx";
import FavoritesPage from "./pages/FavoritePage/FavoritesPage.jsx";


export const router = createBrowserRouter([

    {
        path: "/",
        element: <Layout/>,
        children: [
            {
                index: true,
                element: <MainPage/>
            },
            {
                path: "/basket",
                element: <BasketPage/>
            },
            {
                path: "/favorites",
                element: <FavoritesPage/>
            }
        ]
    }
])