import "./HederFavorite.scss"


function HeaderFavorite({className,children}) {
    return(
        <div className={className}>{children}</div>
    )
}

export default HeaderFavorite