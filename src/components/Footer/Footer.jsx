import "../Footer/Footer.scss"

function Footer() {
    return(
        <footer className='footer'>
            <div className="footer__block">
                <h3>СОЦІАЛЬНІ МЕРЕЖІ</h3>
                <p>Instagram</p>
                <p>Facebook</p>
            </div>
            <div className="footer__block">
                <h3>ІНФОРМАЦІЯ</h3>
                <p>Доставка та оплата</p>
                <p>Договір оферти</p>
                <p>Політика конфіденційності</p>
            </div>
            <div className="footer__block">
                <h3>КОНТАКТИ</h3>
                <p>Номер телефону</p>
                <p>military-storage@gmail.com</p>
            </div>
            <div className="footer__block">
                <h3>АДРЕСА</h3>
                <p>м.Київ, вул.Хрещатик 3</p>
            </div>
        </footer>
    )
}

export default Footer;