import {useEffect} from "react";
import Modal from "../Modal/Modal.jsx";
import ModalWrapper from "../ModalWrapper/ModalWrapper.jsx";
import ModalHeader from "../ModalHeader/ModalHeader.jsx";
import ModalClose from "../ModalClose/ModalClose.jsx";
import ModalBody from "../ModalBody/ModalBody.jsx";
import ModalFooter from "../ModalFooter/ModalFooter.jsx";
import Button from "../../Button/Button.jsx";
import {closeModal} from "../../../features/modal/modalSlice.js";
import {useDispatch} from "react-redux";

function ModalImage({data, bodyQuestion, onConfirm}) {

    useEffect(() => {
        const handleOutsideClick = (event) => {
            if (!event.target.closest('.modal')) {
                closeModal();
            }
        }

        document.addEventListener('mousedown', handleOutsideClick);

        return () => {
            document.removeEventListener('mousedown', handleOutsideClick);
        };
    }, [closeModal]);

    const dispatch = useDispatch();
    const closeModalHandler = () => {
        dispatch(closeModal());
    };


    return (
        <>
            <ModalWrapper>
                <Modal className="modal modal__modal-image">
                    <ModalHeader>
                        <ModalClose onClick={closeModalHandler}/>
                    </ModalHeader>
                    <ModalBody>
                        <img className="modal-body__img-top" src={data.imageUrl} alt="img"/>
                        <h1 className="modal-body__title modal-body__title__modal-image">{data.name}</h1>
                        <p className="modal-body__price">Ціна: {data.price} грн</p>
                        {bodyQuestion}
                    </ModalBody>
                    <ModalFooter className="modal-footer modal-footer__modal-image">
                        <Button type="button" className="btn-modal btn-modal__left-btn" onClick={closeModalHandler}>НІ,
                            ВІДМІНИТИ</Button>
                        <Button type="button" className="btn-modal btn-modal__right-btn" onClick={() => {
                            onConfirm();
                            closeModalHandler();
                        }}>ТАК</Button>
                    </ModalFooter>

                </Modal>
            </ModalWrapper>

        </>
    )

}

export default ModalImage;