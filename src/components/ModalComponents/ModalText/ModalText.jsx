import {useEffect} from "react";
import Modal from "../Modal/Modal.jsx";
import ModalWrapper from "../ModalWrapper/ModalWrapper.jsx";
import ModalHeader from "../ModalHeader/ModalHeader.jsx";
import ModalClose from "../ModalClose/ModalClose.jsx";
import ModalBody from "../ModalBody/ModalBody.jsx";
import ModalFooter from "../ModalFooter/ModalFooter.jsx";
import Button from "../../Button/Button.jsx";


function ModalText({closeModal}) {
    useEffect(() => {
        const handleOutsideClick = (event) => {
            if (!event.target.closest('.modal')) {
                closeModal();
            }
        };

        document.addEventListener('mousedown', handleOutsideClick);

        return () => {
            document.removeEventListener('mousedown', handleOutsideClick);
        };
    }, [closeModal]);

    return (
        <>
            <ModalWrapper>
                <Modal className="modal modal__modal-text">
                    <ModalHeader>
                        <ModalClose onClick={closeModal}/>
                    </ModalHeader>
                    <ModalBody>
                        <h1 className="modal-body modal-body__title ">Add Product “NAME”</h1>
                        <p className="modal-body modal-body__text modal-body__text__modal-text">Description for you
                            product</p>
                    </ModalBody>
                    <ModalFooter className="modal-footer modal-footer__modal-text">
                        <Button type="button" className="btn-modal btn-modal__second-modal-btn">ADD TO FAVORITE</Button>
                    </ModalFooter>
                </Modal>
            </ModalWrapper>

        </>
    )
}

export default ModalText;