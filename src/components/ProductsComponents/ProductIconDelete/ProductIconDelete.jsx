function ProductIconDelete({clickItemDelete}) {


    return (
        <a href="#" onClick={clickItemDelete} title="додати в обране">
            <svg className="product-list__icon-delete" width="30"
                 height="25" fill="none"
                 xmlns="http://www.w3.org/2000/svg">

                <path
                    d="M23.707.293h0a1,1,0,0,0-1.414,0L12,10.586,1.707.293a1,1,0,0,0-1.414,0h0a1,1,0,0,0,0,1.414L10.586,12,.293,22.293a1,1,0,0,0,0,1.414h0a1,1,0,0,0,1.414,0L12,13.414,22.293,23.707a1,1,0,0,0,1.414,0h0a1,1,0,0,0,0-1.414L13.414,12,23.707,1.707A1,1,0,0,0,23.707.293Z"/>
            </svg>

        </a>
    )
}

export default ProductIconDelete;