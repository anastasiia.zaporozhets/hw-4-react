import "../ProductCard/ProductCard.scss"
import PropTypes from "prop-types";
import Button from "../../Button/Button.jsx";
import ProductIconStar from "../ProductIconStar/ProductIconStar.jsx";
import ProductIconDelete from "../ProductIconDelete/ProductIconDelete.jsx";
import {useSelector} from "react-redux";
import {useDispatch} from "react-redux";
import {addToFav, deleteToFAv} from "../../../features/counter/counterSlice.js";
import {openModal} from "../../../features/modal/modalSlice.js";

function ProductCard({
                         item, onBuyClick, showStar = true,
                         showDelete = false, showButtonBuy = true
                     }) {

    const dispatch = useDispatch();
    const favorites = useSelector(state => state.counter.favorite);

    const handleStarClick = (event) => {
        event.preventDefault();
        if (!favorites.includes(item)) {
            dispatch(addToFav(item))
        } else {
            dispatch(deleteToFAv(item.id))
        }
    };

    const openModalHandler = () => {
        dispatch(openModal({modalType: 'image', modalData: item}));
        console.log("OPEN MODAL")
    };

    const isFavorite = favorites.some(favItem => favItem.id === item.id);


    return (
        <div className="product-list__card">
            {showStar && <ProductIconStar
                handleStarClick={handleStarClick}
                isFavorite={isFavorite}

            />}
            {showDelete && <ProductIconDelete clickItemDelete={openModalHandler}/>}

            <img src={item.imageUrl} alt="product" className="product-list__card__img"/>
            <div className="product-list__card__wrapper-text">
                <p className="product-list__card__wrapper-text__name">{item.name}</p>
                <p className="product-list__card__wrapper-text__color-text">Колір: {item.color}</p>
            </div>
            <div className="product-list__card__price-wrapper">
                <p className="product-list__card__price-wrapper__price">Ціна {item.price} грн</p>
            </div>
            <div className="btn-wrapper">
                {showButtonBuy && <Button type="button" className="btn btn__main btn__main__first"
                                          onClick={() => {
                                              onBuyClick(item)
                                          }}>КУПИТИ</Button>}
            </div>
        </div>
    );
}

ProductCard.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.number.isRequired,
        imageUrl: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        article: PropTypes.string.isRequired,
    }).isRequired,
    onBuyClick: PropTypes.func
};

export default ProductCard