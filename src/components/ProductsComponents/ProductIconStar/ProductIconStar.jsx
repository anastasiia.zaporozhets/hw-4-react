
function ProductIconStar({handleStarClick, isFavorite}) {


    return (
        <a href="#" onClick={handleStarClick} title="додати в обране">
            <svg className={`product-list__icon-star ${isFavorite ? 'product-list__icon-storage' : ''}`}
                 width="40"
                 height="35" fill="none"
                 viewBox="0 0 24 24"
                 xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M19.467,23.316,12,17.828,4.533,23.316,7.4,14.453-.063,9H9.151L12,.122,14.849,9h9.213L16.6,14.453Z"
                    stroke="#807D7E" strokeWidth="1.9" strokeLinecap="round" strokeLinejoin="round"/>
            </svg>
        </a>
    )

}

export default ProductIconStar;